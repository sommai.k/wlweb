import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user/user.service';
import { User } from '../../shared/user/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.user = new User();
  }

  ngOnInit() {}

  doLogin() {
    this.userService.login(this.user).subscribe(resp => {
      if (resp.success) {
        this.router.navigate(['private', 'member']);
      } else {
        alert('Login fail...');
      }
    });
  }
}
