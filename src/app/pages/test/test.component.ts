import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  showTitle = true;
  xText = '';
  datas = [1, 2, 3, 4];

  testNumber = 1.8;
  currentDate = new Date();

  @Input()
  txtTitle;

  // @Input()
  // get txtTitle() {
  //   return this.xText;
  // }

  // @Output() txtTitleChange = new EventEmitter();

  // set txtTitle(val) {
  //   this.xText = val;
  //   this.txtTitleChange.emit(this.xText);
  // }

  constructor() {}

  ngOnInit() {}

  greeting(): void {
    this.showTitle = !this.showTitle;
    /*
    if (this.showTitle) {
      this.showTitle = false;
    } else {
      this.showTitle = true;
    }
*/
    console.log('hello form test..');
  }

  onButtonClick() {
    // this.datas.push( this.datas.length + 1);
    this.datas.push(12);
  }

  onDeleteClick(idx){
    this.datas.splice(idx, 1);
  }
}
