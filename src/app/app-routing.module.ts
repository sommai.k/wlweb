import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicZoneComponent } from './pages/public-zone/public-zone.component';
import { LoginComponent } from './pages/login/login.component';
import { MemberComponent } from './pages/member/member.component';
import { PrivateZoneComponent } from './pages/private-zone/private-zone.component';
import { ProjectListComponent } from './pages/project-list/project-list.component';
import { LoginGuard } from './shared/user/login.guard';

const routes: Routes = [{
  path: '',
  component: PublicZoneComponent,
  children: [{
    path: '',
    component: HomeComponent
  }, {
    path: 'login',
    component: LoginComponent
  }]
}, {
  path: 'private',
  component: PrivateZoneComponent,
  canActivate: [ LoginGuard ],
  children: [{
    path: 'member',
    component: MemberComponent
  }, {
    path: 'project-list',
    component: ProjectListComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
