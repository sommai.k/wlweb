import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './pages/test/test.component';
import { TestStringPipe } from './pipes/test-string.pipe';
import { HomeComponent } from './pages/home/home.component';
import { PublicZoneComponent } from './pages/public-zone/public-zone.component';
import { PrivateZoneComponent } from './pages/private-zone/private-zone.component';
import { LoginComponent } from './pages/login/login.component';
import { UserService } from './shared/user/user.service';
import { ProjectListComponent } from './pages/project-list/project-list.component';
import { MemberComponent } from './pages/member/member.component';
import { LoginGuard } from './shared/user/login.guard';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestStringPipe,
    HomeComponent,
    PublicZoneComponent,
    PrivateZoneComponent,
    LoginComponent,
    ProjectListComponent,
    MemberComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [UserService, LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
